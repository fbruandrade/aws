provider "aws" {
	region = "sa-east-1"
}

# Create APP VPC
resource "aws_vpc" "shared-app" {
  cidr_block = "10.170.224.0/23"
  tags = {
    Name = "development"
  }
}

# Create subnet(s)
# Subnets have to be allowed to automatically map public IP addresses for worker nodes
resource "aws_subnet" "app-subnet-1" {
  vpc_id     = aws_vpc.shared-app.id
  cidr_block = "10.170.224.0/24"
  availability_zone = "sa-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "app-subnet-1"
  }
}

resource "aws_subnet" "app-subnet-2" {
  vpc_id     = aws_vpc.shared-app.id
  cidr_block = "10.170.225.0/24"
  availability_zone = "sa-east-1c"
  map_public_ip_on_launch = true

  tags = {
    Name = "app-subnet-2"
  }
}

# Create DBVPC
resource "aws_vpc" "shared-db" {
  cidr_block = "10.170.224.0/23"
  tags = {
    Name = "development"
  }
}

# Create subnet(s)
# Subnets have to be allowed to automatically map public IP addresses for worker nodes
resource "aws_subnet" "db-subnet-1" {
  vpc_id     = aws_vpc.shared-db.id
  cidr_block = "10.170.224.0/24"
  availability_zone = "sa-east-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "db-subnet-1"
  }
}

resource "aws_subnet" "db-subnet-2" {
  vpc_id     = aws_vpc.shared-db.id
  cidr_block = "10.170.225.0/24"
  availability_zone = "sa-east-1c"
  map_public_ip_on_launch = true

  tags = {
    Name = "db-subnet-2"
  }
}

# Create Internet Gateway
resource "aws_internet_gateway" "dev-gw" {
  vpc_id = aws_vpc.shared-app.id

  tags = {
    Name = "shared-app-gw"
  }
}

# Create Route Table
resource "aws_route_table" "app-route-table" {
  vpc_id = aws_vpc.shared-app.id

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.dev-gw.id
    }

  route {
      ipv6_cidr_block        = "::/0"
      gateway_id = aws_internet_gateway.dev-gw.id
    }
  

  tags = {
    Name = "sared-app-rt"
  }
}

# Create Route Table Association for dev1-subnet to dev-rt
resource "aws_route_table_association" "dev1-sub-to-dev-rt" {
  subnet_id      = aws_subnet.app-subnet-1.id
  route_table_id = aws_route_table.app-route-table.id
}

# Create Route Table Association for dev1-subnet to dev-rt
resource "aws_route_table_association" "dev2-sub-to-dev-rt" {
  subnet_id      = aws_subnet.app-subnet-2.id
  route_table_id = aws_route_table.app-route-table.id
}

# Create a security group for HTTPS, HTTP, and SSH
resource "aws_security_group" "allow-web-traffic" {
  name        = "allow_tls"
  description = "Allow web traffic"
  vpc_id      = aws_vpc.shared-app.id

  ingress {
      description      = "HTTPS"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }


  ingress {
      description      = "HTTP"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }

  ingress {
      description      = "SSH"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }

  egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }

  tags = {
    Name = "allow-web"
  }
}

# Create a security group for HTTPS, HTTP, and SSH
resource "aws_security_group" "allow-db-traffic" {
  name        = "allow_tls"
  description = "Allow db traffic"
  vpc_id      = aws_vpc.shared-db.id

  ingress {
      description      = "MySQL"
      from_port        = 3306
      to_port          = 3306
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }


  ingress {
      description      = "PostgreSQL"
      from_port        = 5432
      to_port          = 5432
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }

  ingress {
      description      = "MongoDB"
      from_port        = 27017
      to_port          = 27017
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
    }

  egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }

  tags = {
    Name = "allow-web"
  }
}

# Create a NIC(s)
# resource "aws_network_interface" "dev-server-nic" {
#   subnet_id       = aws_subnet.dev1-subnet.id
#   private_ips     = ["10.170.224.50"]
#   security_groups = [aws_security_group.allow-web-traffic.id]
# }

# # Create Elastic IP
# resource "aws_eip" "one" {
#   vpc                       = true
#   network_interface         = aws_network_interface.dev-server-nic.id
#   associate_with_private_ip = "10.170.224.50"
#   depends_on = [aws_internet_gateway.dev-gw]
# }
