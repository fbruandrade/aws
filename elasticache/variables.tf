variable "namespace" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "subnets" {
  type    = list(string)
  default = ["10.170.224.0/24", "10.170.225.0/24"]
}

variable "cluster_id" {
  type = string
}

variable "node_groups" {
  type    = list(string)
}
