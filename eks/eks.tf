data "aws_eks_cluster" "shared-cluster" {
  name = module.my-cluster.cluster_id
}

data "aws_eks_cluster_auth" "shared-cluster" {
  name = module.my-cluster.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.shared-cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.shared-cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.shared-cluster.token
  #load_config_file       = false
}

module "my-cluster" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = "shared-cluster"
  cluster_version = "1.21"
  subnets         = var.subnets
  vpc_id          = var.vpc_id

# If the instance is too small, you will not have enough available NICs to assign IP addresses to
# all the pods on your instances
  worker_groups = [
    {
      name = "shared-worker-group-1"
      instance_type = "m5.large"
      asg_min_size = 2
      asg_desired_capacity = 2
      asg_max_size  = 2
      additional_security_group_ids = ["aws_security_group.allow-web-traffic.id"]
    }
  ]
}