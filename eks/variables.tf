variable "az" {
  type    = list(string)
  default = ["sa-east-1a", "sa-east-1c"]
}

variable "vpc_cidr_block" {
  type = string
}

variable "eks_cluster_name" {
  type = string
  default = "devops"
}

variable "rds_port" {
  type    = number
  default = 5432
}

variable "subnets" {
  type    = list(string)
  default = ["10.170.224.0/24", "10.170.225.0/24"]
}

variable "vpc_id" {
  type = string
}